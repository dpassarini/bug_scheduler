require 'spec_helper'
require 'csv'
require_relative '../lib/bug'
require_relative '../lib/today_bugs'
require_relative '../lib/line_parser'

describe TodayBugs do
  context 'When receiving a list of bugs' do
    before do 
      @bugs = []
      CSV.read('spec/fixtures/bugs_today.csv')[1..].each do |line|
        bug = LineParser.line_to_data(line) 
        @bugs << bug if bug.valid? 
      end
    end

    it 'should list only the bugs for today, given the rules' do 
      today_bugs = TodayBugs.new(bugs_list: @bugs)
      expect(today_bugs.bugs_for_today.count).to eq(15)
      expect(today_bugs.bugs_list.count).to eq(31)
    end
  end
end