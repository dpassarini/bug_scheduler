require 'spec_helper'
require 'csv'
require_relative '../lib/bug'
require_relative '../lib/today_bugs'
require_relative '../lib/bug_schedules'
require_relative '../lib/line_parser'

describe BugSchedules do
  context 'When receiving a list of bugs of the day' do 
    before do 
      bugs = []
      CSV.read('spec/fixtures/bugs_today.csv')[1..].each do |line|
        bug = LineParser.line_to_data(line) 
        bugs << bug if bug.valid? 

        @today_bugs = TodayBugs.new(bugs_list: bugs)
      end
    end

    it 'should clusterize by estimatives' do 
      bs = BugSchedules.new(bugs_of_day: @today_bugs.bugs_for_today, number_of_schedules: 2)
      expect(bs.schedules.count).to eq(2)
    end
  end

  context 'When receiving a list composed by critical and regular bugs' do
    before do 
      bugs = []
      CSV.read('spec/fixtures/bugs_critical_regular.csv')[1..].each do |line|
        bug = LineParser.line_to_data(line) 
        bugs << bug if bug.valid? 

        @today_bugs = TodayBugs.new(bugs_list: bugs)
      end
    end

    it 'should prioritize the critical ones' do
      bs = BugSchedules.new(bugs_of_day: @today_bugs.bugs_for_today, number_of_schedules: 1)
      
      # all of the bugs are critical
      expect(bs.schedules.first.map{|s| s.critical }.uniq.first).to be_truthy
    end
  end
end
