require 'spec_helper'
require_relative '../lib/bug'

describe Bug do
  describe '#valid?' do 
    context 'When the class initializes' do 
      context 'with invalid params' do 
        it 'should return false' do 
          invalid_bug = Bug.new(title: 'Bug', age: 'Bug', estimate: 'Bug', critical: 'bug')

          expect(invalid_bug.valid?).to be_falsey
        end
      end

      context 'with valid params' do 
        it 'should return true' do 
          invalid_bug = Bug.new(title: 'Bug', age: 1, estimate: 3, critical: false)

          expect(invalid_bug.valid?).to be_truthy
        end
      end
    end
  end
end