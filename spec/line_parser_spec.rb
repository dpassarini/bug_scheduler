require 'spec_helper'
require 'csv'
require_relative '../lib/line_parser'
require_relative '../lib/bug'

describe LineParser do
  describe '#self.line_to_data' do
    context 'When receiving a valid line' do
      it 'should return a Bug object' do
        line = CSV.parse_line('Bug 1,1,7,1') # to keep consistent with the implementation
        bug = LineParser.line_to_data(line)
        
        expect(bug).to be_a(Bug)
        expect(bug.valid?).to be_truthy
      end
    end
  end
end
