require 'csv'
require 'json'
require_relative 'lib/bug'
require_relative 'lib/line_parser'
require_relative 'lib/today_bugs'
require_relative 'lib/bug_schedules'

caminho_arquivo = ARGV[0] || 'bugs.csv'
num_de_agendas = (ARGV[1] || '1').to_i

bugs = []
begin
  CSV.read(caminho_arquivo)[1..].each do |line|
    bug = LineParser.line_to_data(line) 
    bugs << bug if bug.valid? 
  end
rescue
  puts 'Arquivo inválido'
  exit! 
end

today_bugs = TodayBugs.new(bugs_list: bugs)

scheduler = BugSchedules.new(bugs_of_day: today_bugs.bugs_for_today, number_of_schedules: num_de_agendas)

File.open('output/today_bugs.json', 'w'){ |f| f.write(JSON.generate(today_bugs.bugs_for_today)) }

scheduler.schedules.each_with_index do |schedule, i|
  File.open("output/schedule_#{i+1}.json", 'w'){ |f| f.write(JSON.generate(schedule)) }
end
