# Desafio Festalab

##Instruções para rodar o desafio

### Versão da linguagem Ruby
Esse software foi desenvolvido em Ruby, na versão 3.0

### Obtendo o software
Clone esse repositório. Entre no diretório clonado:
```sh
$ cd bug_scheduler
```
E baixe a dependência usando o comando bundle:
```sh
$ bundle
```
Após essa etapa, já é possível rodar os testes, usando o comando abaixo:
```sh
$ rspec
```

##Utilizando o software
O método de entrada escolhido é arquivo CSV, nesse formato:
```
titulo,idade,estimativa,critico
Bug Today 1,1,2,1
Bug Today 2,1,2,1
Bug Today 3,1,2,1
Bug Today 4,1,2,0
Bug Today 5,1,2,0
```

Utilizar cabeçalho.

A saída será em arquivos JSON, pois é mais fácil gerar esses arquivos.

No diretório onde esse repositório foi clonado, rode o seguinte comando:
```sh
$ ruby bootstrap.rb /caminho/do/arquivo/bug 2
```
O primeiro argumento é o arquivo de agendas. O segundo é quantas agendas de até 8 horas de trabalho serão geradas.

Para usar o arquivo de exemplo, rode:
```sh
$ ruby bootstrap.rb 
```

Os bugs do dia e as agendas serão geradas no diretório output.