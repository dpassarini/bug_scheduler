require_relative 'bug'

class BugSchedules
  attr_reader :bugs_of_day, :number_of_schedules, :bugs_by_estimative, :critical_bugs_by_estimative, :schedules

  MAXIMUM_WORKLOAD = 8
  
  def initialize(bugs_of_day:, number_of_schedules:)
    @bugs_of_day = bugs_of_day
    @number_of_schedules = number_of_schedules
    @schedules = []
    clusterize_bugs_by_estimative
    number_of_schedules.times{ create_schedules }
  end

  private

  def clusterize_bugs_by_estimative
    return @bugs_by_estimative if @bugs_by_estimative != [] && @bugs_by_estimative.is_a?(Hash)

    @bugs_by_estimative = {}
    @critical_bugs_by_estimative = {}

    @bugs_of_day.each do |bug|
      bug.critical ? add_bug_to_custer(bug, @critical_bugs_by_estimative) : add_bug_to_custer(bug, @bugs_by_estimative)
    end
  end

  def add_bug_to_custer(bug, cluster)
    key = bug.estimate.to_i
    if cluster[key].nil?
      cluster[key] = [bug]
    else
      cluster[key] << bug
    end
  end

  def create_schedules
    return if (@critical_bugs_by_estimative.nil? || @critical_bugs_by_estimative == []) && (@bugs_by_estimative.nil? || @bugs_by_estimative == [])
    
    schedule = []
    schedule_workload = 0
    
    # first the critical bugs
    current_schedule, updated_workload = fill_schedule(@critical_bugs_by_estimative, schedule, schedule_workload)
    
    
    # then the regular ones
    current_schedule, _ = fill_schedule(@bugs_by_estimative, current_schedule, updated_workload)

    @schedules << current_schedule
  end

  def fill_schedule(bug_list, schedule, workload)
    keys = bug_list.keys.sort.reverse
    keys.each do |key|
      break if workload >= MAXIMUM_WORKLOAD
      bug_list.each do |i, bugs|
        break if workload >= MAXIMUM_WORKLOAD
        bugs.each_with_index do |bug, j|
          if bug.estimate + workload <= MAXIMUM_WORKLOAD
            schedule << bug
            workload += bug.estimate 
            bugs[j] = nil
          else
            bugs.compact!
            break
          end
        end
      end
    end

    return schedule, workload
  end
end
