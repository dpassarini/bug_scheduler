require_relative 'bug'

class TodayBugs
  attr_reader :bugs_list, :bugs_for_today

  BUG_LIMIT_DATE = 3
  CRITICAL_BUG_LIMIT_DATE = 1

  def initialize(bugs_list:)
    @bugs_for_today = []
    @bugs_list = bugs_list
    extract_bugs
  end

  private

  def extract_bugs
    bugs_list.each do |bug|
      if bug.critical 
        @bugs_for_today << bug if bug.age >= CRITICAL_BUG_LIMIT_DATE
      else
        @bugs_for_today << bug if bug.age >= BUG_LIMIT_DATE
      end
    end
  end
end