class Bug
  attr_reader :title, :age, :estimate, :critical

  MAXIMUM_ESTIMATE = 8

  def initialize(title:, age:, estimate:, critical:)
    @title = title
    @age = age
    @estimate = estimate
    @critical = critical
  end

  def valid?
    return false if title.nil? || age.nil? || estimate.nil? || critical.nil?
    return false if !age.is_a?(Integer) || !estimate.is_a?(Integer)
    return false if estimate > MAXIMUM_ESTIMATE

    true
  end
end
