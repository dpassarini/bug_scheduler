require 'csv'
require_relative 'bug'

class LineParser
  def self.line_to_data(line)
    Bug.new(title: line[0], age: line[1].to_i, estimate: line[2].to_i, critical: line[3]=='1')
  end
end
